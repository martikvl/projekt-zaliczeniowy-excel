using NUnit.Framework;
using System;
using System.Threading;
using FlaUI.Core;
using FlaUI.Core.AutomationElements;
using FlaUI.Core.Conditions;
using FlaUI.Core.Input;
using FlaUI.Core.Tools;
using FlaUI.Core.WindowsAPI;
using FlaUI.UIA3;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FlaUI.Core.Definitions;

namespace Window_NUnit_Testowanie
{
    public class Tests
    {
        [SetUp]
        public void Setup()
        {
        }

        //Test sprawdza czy suma policzona z dw�ch kom�rek zgadza si� z w�a�ciwym wynikiem
        [Test]
        public void SumTest()
        {
            Thread.Sleep(5000);
            var msApplication = Application.Launch(@"C:\Program Files\Microsoft Office 15\root\office15\EXCEL.exe");

            Thread.Sleep(3000);

            var automation = new UIA3Automation();
            var mainWindow = msApplication.GetMainWindow(automation);

            ConditionFactory cf = new ConditionFactory(new UIA3PropertyLibrary());
            mainWindow.FindFirstDescendant(cf.ByName("Pusty skoroszyt")).AsButton().Click();

            Thread.Sleep(2000);

            int nr1 = 5;
            int nr2 = 7;
            int sum = 5 + 7;

            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter(nr1.ToString());
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter(nr2.ToString());
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("=A1+A2");
            Keyboard.Press(VirtualKeyShort.ENTER);
            
            var tabitem = mainWindow.FindFirstDescendant(cf.ByName("A3")).AsTabItem();
            String result = tabitem.Patterns.Value.Pattern.Value;

            String expected = sum.ToString();
            Assert.IsTrue(result.Equals(expected));
            
            msApplication.Close();
        }

        //Test sprawdza czy zapisany arkusz z ostatnio u�ywanych otwiera si� poprawnie
        [Test]
        public void OpenSavedTest()
        {
            Thread.Sleep(5000);
            var msApplication = Application.Launch(@"C:\Program Files\Microsoft Office 15\root\office15\EXCEL.exe");

            Thread.Sleep(3000);

            var automation = new UIA3Automation();
            var mainWindow = msApplication.GetMainWindow(automation);

            String expected = "dane_excel";
            ConditionFactory cf = new ConditionFactory(new UIA3PropertyLibrary());
            mainWindow.FindFirstDescendant(cf.ByName(expected).And(cf.ByControlType(ControlType.Hyperlink))).AsButton().Click();

            Thread.Sleep(2000);

            var titlebar = mainWindow.FindFirstDescendant(cf.ByControlType(ControlType.TitleBar));
            String result = titlebar.Properties.HelpText.ToString();

            Assert.IsTrue(result.Equals(expected));

            msApplication.Close();
        }

        //Test sprawdza czy poprawnie wstawia si� wykres dla wprowadzonych najpierw danych
        [Test]
        public void ChartTest()
        {
            Thread.Sleep(5000);
            var msApplication = Application.Launch(@"C:\Program Files\Microsoft Office 15\root\office15\EXCEL.exe");

            Thread.Sleep(3000);

            var automation = new UIA3Automation();
            var mainWindow = msApplication.GetMainWindow(automation);

            ConditionFactory cf = new ConditionFactory(new UIA3PropertyLibrary());
            mainWindow.FindFirstDescendant(cf.ByName("Pusty skoroszyt")).AsButton().Click();

            Thread.Sleep(2000);

            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("x");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("1");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("2");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("3");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("4");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("5");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("6");
            Keyboard.Press(VirtualKeyShort.ENTER);
            Keyboard.Type(VirtualKeyShort.RIGHT);
            for (int j = 0; j < 7; j++)
            {
                Keyboard.Type(VirtualKeyShort.UP);
            }
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("y");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("10");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("7");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("13");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("12");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("9");
            Keyboard.Press(VirtualKeyShort.ENTER);
            mainWindow.FindFirstDescendant(cf.ByName("Siatka")).AsTextBox().Enter("8");
            Keyboard.Press(VirtualKeyShort.ENTER);

            mainWindow.FindFirstDescendant(cf.ByName("Wstawianie")).AsButton().Click();
            Thread.Sleep(1000);
            mainWindow.FindFirstDescendant(cf.ByName("Wstaw wykres kolumnowy")).AsButton().Click();
            Thread.Sleep(1000);
            mainWindow.FindFirstDescendant(cf.ByName("Kolumnowy grupowany")).AsButton().Click();
            Thread.Sleep(2000);

            bool isChart = mainWindow.FindFirstDescendant(cf.ByName("Wykres 1")).IsAvailable;

            Assert.IsTrue(isChart);
            
            msApplication.Close();
        }
    }
}
